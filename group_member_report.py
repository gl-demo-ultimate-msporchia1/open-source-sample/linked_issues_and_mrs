#!/usr/bin/env python3

import json
import csv
import argparse
import gitlab
import urllib3
from datetime import datetime
from mako.template import Template


class Group_Member_Reporter:

    gitlab_url = "https://gitlab.com/"
    report_group = None
    all = False
    with_inherited = False
    gl = None
    self_managed = False
    groups = []
    projects = []
    project_map = {}
    group_map = {}
    group_share_map = {}
    errors = []

    def __init__(self, args):
        if args.gitlaburl:
            self.gitlab_url = (
                args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
            )
        self.all = args.all
        self.with_inherited = args.with_inherited
        if self.gitlab_url != "https://gitlab.com/":
            self.self_managed = True
            if args.group:
                print(
                    "Discarding requested group, self-managed mode only supports all groups"
                )
        else:
            if not args.group:
                print("ERROR: need to specify a group to query for https://gitlab.com/")
                exit()
            else:
                self.report_group = args.group

        if args.disable_certificate_verification:
            print("WARN: SSL verification is disabled.")
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self.gl = gitlab.Gitlab(
            self.gitlab_url,
            private_token=args.token,
            retry_transient_errors=True,
            ssl_verify=not args.disable_certificate_verification,
        )
        # verify user and API connection
        top_groups = []
        if self.self_managed:
            # self.verify_sm_user()
            try:
                top_groups = self.gl.groups.list(iterator=True, top_level_only=True)
            except Exception as e:
                print("ERROR: Could not retrieve top level groups")
                print(e)
                exit(1)
            for top_group in top_groups:
                self.groups.append(self.gl.groups.get(top_group.id))
                self.get_subgroups(top_group)
                self.group_map = {group.id: group for group in self.groups}
            self.projects = self.get_projects()
            self.project_map = {project.id: project for project in self.projects}
        else:
            try:
                self.report_group = self.gl.groups.get(self.report_group)
                if "/" in self.report_group.full_path:
                    print(
                        "WARN: Subgroup report not supported, defaulting to top-level group %s instead"
                        % self.report_group.full_path[
                            0 : self.report_group.full_path.find("/")
                        ]
                    )
                    self.report_group = self.gl.groups.get(
                        self.report_group.full_path[
                            0 : self.report_group.full_path.find("/")
                        ]
                    )
                self.verify_saas_user()
            except Exception as e:
                print("ERROR: Could not retrieve group %s" % self.report_group)
                print(e)
                exit(1)
            self.groups = [self.report_group]
            self.get_subgroups(self.report_group)
            self.projects = self.get_projects(self.report_group)
            self.projects = self.get_projects(self.groups[1])
        self.make_group_share_map()

    def get_subgroups(self, group):
        print("Getting subgroups for group %s" % group.full_path)
        subgroups = group.subgroups.list(iterator=True)
        for subgroup in subgroups:
            subgroup_object = self.gl.groups.get(subgroup.id)
            self.groups.append(subgroup_object)
            self.get_subgroups(subgroup_object)

    def get_projects(self, group=None):
        projects = []
        if group:
            print("Getting projects for group %s" % group.full_path)
            group_projects = group.projects.list(include_subgroups=True, iterator=True)
            for i, project in enumerate(group_projects):
                projects.append(self.gl.projects.get(project.id))
        else:
            print("Getting all projects")
            project_objects = self.gl.projects.list(iterator=True)
            for project in project_objects:
                projects.append(project)
        return projects

    def make_group_share_map(self):
        print("Resolving shared groups.")
        for project in self.projects:
            for shared_group in project.shared_with_groups:
                if not self.self_managed:
                    shared_top_level_group = shared_group["group_full_path"][
                        0 : shared_group["group_full_path"].find("/")
                    ]
                    if shared_top_level_group != self.report_group.path:
                        print(
                            "WARN: %s was shared with group %s outside the namespace. This can import additional billable members."
                            % (
                                project.path_with_namespace,
                                shared_group["group_full_path"],
                            )
                        )
                        self.errors.append(
                            "WARN: %s was shared with group %s outside the namespace. This can import additional billable members."
                            % (
                                project.path_with_namespace,
                                shared_group["group_full_path"],
                            )
                        )
                share = {
                    "id": project.id,
                    "type": "project",
                    "membership_type": "shared",
                    "path": project.path_with_namespace,
                    "url": project.web_url + "/-/project_members",
                    "access_level": shared_group["group_access_level"],
                    "shared_via": shared_group["group_id"],
                    "shared_path": shared_group["group_full_path"],
                    "shared_url": self.gitlab_url
                    + "/groups/"
                    + shared_group["group_full_path"]
                    + "/-/group_members",
                }
                if shared_group["group_id"] in self.group_share_map:
                    self.group_share_map[shared_group["group_id"]].append(share)
                else:
                    self.group_share_map[shared_group["group_id"]] = [share]
        for group in self.groups:
            if "shared_with_groups" in group.attributes:
                for shared_group in group.shared_with_groups:
                    if not self.self_managed:
                        shared_top_level_group = shared_group["group_full_path"][
                            0 : shared_group["group_full_path"].find("/")
                        ]
                        if shared_top_level_group != self.report_group.path:
                            print(
                                "WARN: %s was shared with group %s outside the namespace. This can import additional billable members."
                                % (group.full_path, shared_group["group_full_path"])
                            )
                            self.errors.append(
                                "WARN: %s was shared with group %s outside the namespace. This can import additional billable members."
                                % (group.full_path, shared_group["group_full_path"])
                            )
                    share = {
                        "id": group.id,
                        "type": "group",
                        "membership_type": "shared",
                        "path": group.full_path,
                        "url": group.web_url + "/-/group_members",
                        "access_level": shared_group["group_access_level"],
                        "shared_via": shared_group["group_id"],
                        "shared_path": shared_group["group_full_path"],
                        "shared_url": self.gitlab_url
                        + "/groups/"
                        + shared_group["group_full_path"]
                        + "/-/group_members",
                    }
                    if shared_group["group_id"] in self.group_share_map:
                        self.group_share_map[shared_group["group_id"]].append(share)
                    else:
                        self.group_share_map[shared_group["group_id"]] = [share]

    def get_users_memberships(self):
        print("Getting user memberships")
        users = None
        if self.self_managed:
            users = self.gl.users.list(iterator=True)
        else:
            users = self.report_group.billable_members.list(iterator=True)
        userlist = []
        for user in users:
            user_object = user.attributes
            if "using_license_seat" not in user_object:
                user_object["using_license_seat"] = None
            if self.self_managed:
                user_object["last_login_at"] = user_object["last_sign_in_at"]
                if not self.all and not user_object["using_license_seat"]:
                    continue
            else:
                user_object["using_license_seat"] = True
            memberships = [
                membership.attributes
                for membership in user.memberships.list(iterator=True)
            ]
            user_object["memberships"] = memberships
            userlist.append(user_object)
        return userlist

    def consolidate_memberships(self, users):
        for user in users:
            result_memberships = []
            user["highest_access_level"] = 0
            for membership in user["memberships"]:
                # user is member in a group that has been shared to other groups / projects. Add this shared access to their memberships
                if self.self_managed and not self.with_inherited:
                    membership = self.enhance_sm_membership(membership)
                source_type = (
                    "project"
                    if membership["source_members_url"].endswith("/project_members")
                    else "group"
                )
                if (
                    membership["access_level"]["integer_value"]
                    > user["highest_access_level"]
                ):
                    user["highest_access_level"] = membership["access_level"][
                        "integer_value"
                    ]
                result_membership = {
                    "id": membership["source_id"],
                    "type": source_type,
                    "membership_type": "direct",
                    "path": membership["source_full_name"].lower().replace(" ", ""),
                    "url": membership["source_members_url"],
                    "access_level": membership["access_level"]["integer_value"],
                }
                result_memberships.append(result_membership)
                if membership["source_id"] in self.group_share_map:
                    shared = self.group_share_map[membership["source_id"]]
                    result_memberships.extend(shared)
            result_memberships = sorted(
                result_memberships,
                key=lambda d: (d["path"], d["access_level"]),
                reverse=True,
            )
            user["memberships"] = result_memberships
        if not self.all:
            users = [user for user in users if user["using_license_seat"]]
        return users

    # align result of SM user memberships API with SaaS billable users API
    def enhance_sm_membership(self, membership):
        enhanced_membership = membership.copy()
        enhanced_membership["access_level"] = {
            "integer_value": membership["access_level"]
        }
        if membership["source_type"] == "Project":
            if membership["source_id"] in self.project_map:
                enhanced_membership["source_members_url"] = (
                    self.project_map[membership["source_id"]].web_url
                    + "/-/project_members"
                )
                enhanced_membership["source_full_name"] = self.project_map[
                    membership["source_id"]
                ].path_with_namespace
            else:
                print(
                    "Project %s not found in crawled projects. Your token may not have access"
                    % membership["source_id"]
                )
                enhanced_membership["source_members_url"] = (
                    "http://example.org/unknown_project/" + str(membership["source_id"])
                )
                enhanced_membership["source_full_name"] = "Unknown project " + str(
                    membership["source_id"]
                )
        else:
            if membership["source_id"] in self.group_map:
                enhanced_membership["source_members_url"] = (
                    self.group_map[membership["source_id"]].web_url + "/-/group_members"
                )
                enhanced_membership["source_full_name"] = self.group_map[
                    membership["source_id"]
                ].full_path
            else:
                print(
                    "Groups %s not found in crawled groups. Your token may not have access"
                    % membership["source_id"]
                )
                enhanced_membership["source_members_url"] = (
                    "http://example.org/unknown_group/" + str(membership["source_id"])
                )
                enhanced_membership["source_full_name"] = "Unknown group " + str(
                    membership["source_id"]
                )
        return enhanced_membership

    # legacy algorithm to retrieve all direct memberships from groups and projects
    def get_granular_memberships(self):
        memberships = []
        for group in self.groups:
            print("Getting members of %s" % group.full_path)
            members = None
            if self.with_inherited:
                members = group.members_all.list(iterator=True)
            else:
                members = group.members.list(iterator=True)
            for member in members:
                membership = member.attributes
                membership["type"] = "group"
                membership["access_level"] = {
                    "integer_value": membership["access_level"]
                }
                membership["source_id"] = group.id
                membership["source_full_name"] = group.full_path
                membership["membership_type"] = "direct"
                membership["source_members_url"] = group.web_url + "/-/group_members"
                memberships.append(membership)
        for project in self.projects:
            print("Getting members of %s" % project.path_with_namespace)
            members = None
            if self.with_inherited:
                members = project.members_all.list(iterator=True)
            else:
                members = project.members.list(iterator=True)
            for member in members:
                membership = member.attributes
                membership["type"] = "project"
                membership["access_level"] = {
                    "integer_value": membership["access_level"]
                }
                membership["source_id"] = project.id
                membership["source_full_name"] = project.path_with_namespace
                membership["membership_type"] = "direct"
                membership["source_members_url"] = (
                    project.web_url + "/-/project_members"
                )
                memberships.append(membership)
        usermap = {}
        for membership in memberships:
            # print(membership)
            if membership["id"] in usermap:
                usermap[membership["id"]]["memberships"].append(membership)
            else:
                print("Getting user data for %s" % membership["username"])
                user = self.gl.users.get(membership["id"])
                userdata = user.attributes
                # print(userdata)
                userdata["email"] = "" if "email" not in userdata else userdata["email"]
                userdata["last_activity_on"] = (
                    ""
                    if "last_activity_on" not in userdata
                    else userdata["last_activity_on"]
                )
                userdata["last_login_at"] = (
                    "" if "last_login_at" not in userdata else userdata["last_login_at"]
                )
                userdata["using_license_seat"] = (
                    ""
                    if "using_license_seat" not in userdata
                    else userdata["using_license_seat"]
                )
                userdata["state"] = "" if "state" not in userdata else userdata["state"]
                # print("Adding %s to usermap" % membership["id"])
                usermap[membership["id"]] = userdata
                usermap[membership["id"]]["memberships"] = [membership]

        if not self.self_managed:
            billable_member_data = self.report_group.billable_members.list(
                iterator=True
            )
            for user in billable_member_data:
                user = user.attributes
                if user["id"] in usermap:
                    usermap[user["id"]]["email"] = user["email"]
                    usermap[user["id"]]["last_activity_on"] = user["last_activity_on"]
                    usermap[user["id"]]["last_login_at"] = user["last_login_at"]
                    usermap[user["id"]]["using_license_seat"] = True
                    usermap[user["id"]]["state"] = user["state"]
                else:
                    print(
                        "WARN: Found billable member that is not a member of groups and projects in this group %s."
                        % user["username"]
                    )

        return list(usermap.values())

    def write_csv(self, users):
        reportdate = datetime.now().strftime("%Y-%m-%d")
        filename = "user_report_" + reportdate + ".csv"
        with open("public/" + filename, "w") as user_report:
            writer = csv.writer(user_report)
            fields = [
                "username",
                "name",
                "email",
                "highest_access_level",
                "last_activity_on",
                "last_login_at",
                "state",
                "using_license_seat",
            ]
            header = fields.copy()
            header.extend(["member_of", "access_level", "type", "source"])
            writer.writerow(header)
            for user in users:
                row = []
                for field in fields:
                    row.append(user[field])
                if not user["memberships"]:
                    row.extend(["", "", "", ""])
                    writer.writerow(row)
                    continue
                for membership in user["memberships"]:
                    membership_row = row.copy()
                    membership_row.append(membership["path"])
                    membership_row.append(membership["access_level"])
                    membership_row.append(membership["membership_type"])
                    if membership["membership_type"] == "shared":
                        membership_row.append(membership["shared_path"])
                    else:
                        membership_row.append(membership["path"])
                    writer.writerow(membership_row)

    def write_json(self, users):
        reportdate = datetime.now().strftime("%Y-%m-%d")
        filename = "user_report_" + reportdate + ".json"
        with open("public/" + filename, "w") as user_report:
            json.dump(users, user_report)

    def write_html(self, userlist):
        mytemplate = Template(filename="template/index.html")
        report_group = ""
        if self.report_group:
            report_group = self.report_group.full_path
        with open("public/index.html", "w") as outfile:
            outfile.write(
                mytemplate.render(
                    users=userlist,
                    gitlab=self.gitlab_url,
                    group=report_group,
                    reportdate=datetime.now().strftime("%Y-%m-%d"),
                    errors=self.errors,
                )
            )

    def verify_sm_user(self):
        try:
            # fail early by not retrying transient errors, which shouldn't be necessary
            self.gl.auth()
            currentuser = self.gl.user
            if not currentuser.attributes.get("is_admin"):
                print("Error: Token does not have Admin credentials. Stopping.")
                exit(1)
        except Exception as e:
            print("Error: Can not evaluate token user. Stopping.")
            print(e)
            exit(1)

    def verify_saas_user(self):
        try:
            # fail early by not retrying transient errors, which shouldn't be necessary
            self.gl.auth()
            currentuser = self.gl.user
            reportgroup_members = self.report_group.members.list(iterator=True)
            is_owner = False
            for member in reportgroup_members:
                if member.id == currentuser.id:
                    is_owner = True
            if not is_owner:
                user_object = self.gl.users.get(currentuser.id)
                if not user_object.attributes.get("is_admin"):
                    print(
                        "Error: Token for user %s does not belong to group Owner of %s. Stopping."
                        % (currentuser.username, self.report_group.full_path)
                    )
                    exit()
        except Exception as e:
            print("Error: Can not evaluate token user. Stopping.")
            print(e)
            exit()

    def list_mrs(self):
        try:
            self.gl.auth()
            opened_merge_requests = []
            for i, group in enumerate(self.groups):
                if i > 1:
                    break
                print("Trying group %s" % (group.id))
                merge_requests_in_group = group.mergerequests.list(
                    state="all",
                    scope="all",
                    group_id=group.id,
                    iterator=True,
                    per_page=10,
                )
                opened_mrs = []
                for j, mr in enumerate(merge_requests_in_group):
                    if j > 10:
                        break
                    print(
                        "Found MR %s, with iid %s and weburl: %s"
                        % (mr.id, mr.iid, mr.web_url)
                    )
                    opened_mrs.append(
                        {
                            "group_id": group.id,
                            "project_id": mr.project_id,
                            "mr_iid": mr.iid,
                            "mr_web_url": mr.web_url,
                            "mr_state": mr.state,
                        }
                    )
                opened_merge_requests.append(opened_mrs)

            print("Finished cycling, returning")
            print(opened_merge_requests)
            return opened_merge_requests
        except Exception as e:
            print("Generic error, see details")
            print(e)
            exit()

    def verify_linked_issues(self):
        try:
            self.gl.auth()
            linked_issues_return = []
            numbers_of_projects = self.projects.len()
            for i, project in enumerate(self.projects):
                print(
                    "Trying project %s, %s of %s" % (project.id, i, numbers_of_projects)
                )
                issues_in_project = project.issues.list(
                    scope="all", iterator=True, per_page=10
                )
                linked_issues_for_issue = []
                for issue in issues_in_project:
                    # self.gl.enable_debug()
                    print("Issue: %s from project %s" % (issue.iid, project.id))
                    linked_issues = issue.links.list()
                    if not linked_issues:
                        continue

                    for linked_issue in linked_issues:
                        # print("Linked issue: %s" % (linked_issue.iid))
                        linked_issue_record = {
                            "source_project_id": project.id,
                            "source_project_name": project.name,
                            "source_issue_iid": issue.iid,
                            "target_project_id": linked_issue.project_id,
                            "target_project_name": self.gl.projects.get(
                                linked_issue.project_id
                            ).name,
                            "target_issue_iid": linked_issue.iid,
                            "link_type": linked_issue.link_type,
                        }
                        linked_issues_return.append(linked_issue_record)

            print("Finished cycling, returning")
            return linked_issues_return
        except Exception as e:
            print("Generic error, see details")
            print(e)
            exit()


parser = argparse.ArgumentParser(description="Create report for GitLab group members")
parser.add_argument(
    "-u",
    "--gitlaburl",
    help="Optional instance URL, default to https://gitlab.com/",
    default="https://gitlab.com/",
)
parser.add_argument(
    "-g", "--group", help="Group to report on. Not needed for self-managed."
)
parser.add_argument(
    "--all",
    help="Show all users for self-managed, otherwise report is limited to billable users",
    action="store_true",
)
parser.add_argument(
    "--with_inherited",
    help="Show granular memberships including inherited",
    action="store_true",
)
parser.add_argument(
    "token",
    help="Token with API scope able to read all groups to be reported on. Either admin or group owner token.",
)
parser.add_argument(
    "--disable_certificate_verification",
    help="Disable SSL certificate verification",
    action="store_true",
)
args = parser.parse_args()

reporter = Group_Member_Reporter(args)

# inherited mode requires granular memberships
# if reporter.with_inherited:
#     userlist = reporter.get_granular_memberships()
# #getting all on saas requires granular memberships
# elif not reporter.self_managed and reporter.all:
#     userlist = reporter.get_granular_memberships()
# else:
#     userlist = reporter.get_users_memberships()

# Get the list of opened merge requests

merge_requests = reporter.list_mrs()

# Get all the issues containing linked issues

# linked_issues = reporter.verify_linked_issues()


# create a function that parses each item in the array and prints all its value, iterating over keys
def print_items(items):
    csv_row = ""
    for key, value in items.items():
        csv_row += f"{value},"
    csv_row = csv_row[:-1]
    return csv_row


printable_mrs = []

printable_linked_issues = []
# print(linked_issues)

# userlist = reporter.consolidate_memberships(userlist)
# userlist = sorted(userlist, key=lambda d: d['username'])

# stamp for file txt and csv
# linked_issues = reporter.verify_linked_issues()
# for linked_issue in linked_issues:
#    printable_linked_issues.append(print_items(linked_issue))
# #write file for less
# with open('linked_issue_for_less.txt', 'w', newline='') as fileless:
#   writer= csv.writer(fileless,delimiter='\t', quoting=csv.QUOTE_NONE)
#   for linked in linked_issues:
#     writer.writerow([linked])

# # write file CSV
# with open('linked_issue_for_CSV.csv', 'w', newline='') as csvfile:
#   writer= csv.writer(csvfile,delimiter='\t', quoting=csv.QUOTE_NONE)
#   for linked_csv in printable_linked_issues:
#     writer.writerow([linked_csv])

for mrs_per_projects in merge_requests:
    for merge_request in mrs_per_projects:
        printable_mrs.append(print_items(merge_request))


with open("mrs_for_CSV.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter="\t", quoting=csv.QUOTE_NONE)
    for mr_csv in printable_mrs:
        writer.writerow([mr_csv])


# reporter.write_csv(userlist)
# reporter.write_json(userlist)
# reporter.write_html(userlist)
for error in reporter.errors:
    print(error)
